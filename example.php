<?php

$tasks = taskGenerator(100);
$birthControl = 5;
$onGoingTasks = [];

while (!empty($tasks)) {
    $task = reset($tasks);
    $pid = pcntl_fork();
    if (-1 === $pid) {
        die("error :'(");
    } elseif (0 === $pid) {
        doSomething($task);
        exit();
    } else {
        $onGoingTasks[$task] = $pid;
        $tasks = removeFirstTask($tasks);
        print "pid=" . $pid . ", task=".$task.", todo=" . count($tasks) . ", current=".count($onGoingTasks)."\n";
        if (count($onGoingTasks) === $birthControl) {
            $endedPid = pcntl_wait($status);

            foreach ($onGoingTasks as $task => $pid) {
                if ($pid === $endedPid) {
                    unset($onGoingTasks[$task]);
                }
            }
        }
    }
}

while(pcntl_wait($status) !== -1);

print "all tasks finished !\n";

function removeFirstTask(array $tasks) {
    if (count($tasks) > 1) {
        array_splice($tasks, 0, 1);
    } else {
        $tasks = [];
    }

    return $tasks;
}

function doSomething(string $task) {
    sleep(rand(1, 10));
    print $task." finished\n";
}

function taskGenerator(int $nb): array {
    $tasks = [];
    $i = 1;
    while ($i <= $nb) {
        $tasks[] = "task".$i;
        $i++;
    }

    return $tasks;
}